const fs = require('fs');
const path = require('path');

const handleError = (err, res) => {
    switch(err.status){
        case 400:
            res.status(400).json({ message: err.message });
        break;
        default:
            res.status(500).json({ message: "Server error" });
        break;
    }
};

const correctExtension = (filename) => {
    if(!filename.match(/[^:\/\\ \|\*\?><\"]*[\.](log|txt|json|yaml|xml|js|)$/)) {
        const err = new Error(`Unexpected extension`);
        err.status = 400;
        throw err;
    }
    return true;
};

exports.index = (req, res) => {
    res.status(200).json({ message: 'Hi, it`s my API!' });
};

exports.create_file = (req, res) => {
    const expectedParams = ['filename', 'content'];

    const correctParams = (paramNames) => {
        for(let name of paramNames) {
            if(!req.body[name]) {
                const err = new Error(`Please specify ${name} parameter`);
                err.status = 400;
                throw err;
            }

            if(typeof req.body[name] !== 'string') {
                const err = new Error(`incorrect parameter type`);
                err.status = 400;
                throw err;
            }
        }
        return true;
    };

    try {
        correctParams(expectedParams);

        const { filename, content } = req.body;

        correctExtension(filename);

        fs.writeFile(path.join(__dirname, '../', 'files', filename), content, err => {
            if(err) {
                err.status = 500;
                throw err;
            }

            res.status(200).json({ "message": "File created successfully" });
        });
    }catch(err) {
        handleError(err, res);
    }
};

exports.get_all_files = (req, res) => {
    try {
        if(Object.keys(req.query).length !== 0) {
            const err = new Error("Client error");
            err.status = 400;
            throw err;
        }
        
        fs.readdir(path.join(__dirname, '../', 'files'), (err, files) => {
            if (err) {
                err.status = 500;
                throw err;
            } 

            res.status(200).json({"message": "Success", files: files});
        });
    }catch(err) {
        handleError(err, res);
    }
};

exports.get_specific_file = (req, res) => {
    try {
        if(Object.keys(req.params).length === 0) {
            const err = new Error("Select a file");
            err.status = 400;
            throw err;
        }

        const { filename } = req.params;

        if(!correctExtension(filename)) {
            const err = new Error("Incorrect file extension");
            err.status = 400;
            throw err;
        }

        const splitedName = filename.split('.');
        const extension = splitedName[splitedName.length - 1];

        fs.readdir(path.join(__dirname, '../', 'files'), (err, files) => {
            if (err) {
                err.status = 500;
                throw err;
            } 

            const index = files.indexOf(filename);

            if(index < 0) {
                const err = new Error(`No file with ${filename} filename found`);
                err.status = 400;
                throw err;
            }

            const filePath = path.join(__dirname, '../', 'files', filename);

            const fileStat = fs.promises.stat(filePath, (err, stats) => {
                if (err) {
                    err.status = 500;
                    throw err;
                }
            });

            const fileContent = fs.promises.readFile(filePath, 'utf8', (err, content) => {
                if (err) {
                    err.status = 500;
                    throw err;
                }

                return content;
            });

            Promise.all([fileStat, fileContent]).then(([stats, content]) => {
                res.status(200).json(
                    {
                        "message": "Success",
                        "filename": filename,
                        "content": content,
                        "extension": extension,
                        "uploadedDate": stats.birthtime
                    }
                );
            });
        });
    }catch(err) {
        handleError(err, res);
    }
};