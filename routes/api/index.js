const { Router } = require('express');
const router = Router();

// Controllers
const api_controller = require('../../controller');

// Routes
router.get('/', api_controller.index);

router.post('/files', api_controller.create_file);
router.get('/files', api_controller.get_all_files);
router.get('/files/:filename', api_controller.get_specific_file);

module.exports = router;
