const express = require('express');
const app = express();
const PORT = process.env.PORT || 8080;

// Parser
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//Routes obj
const routes = {
    home: require('./routes/home'),
    api: require('./routes/api')
};

//Register routes
app.use('/', routes.home);
app.use('/api', routes.api);


//Listen port
app.listen(PORT, () => {
    console.log(`Server started on port ${PORT}`);
});